<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGolferTournamentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('golfer_tournament', function (Blueprint $table) {
            $table->increments('id');
            $table->string('golfer_id');
            $table->string('tournament_id');
            $table->string('place')->nullable();
            $table->integer('earnings')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('golfer_tournament');
    }
}
