<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueGolferTournament extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('golfer_tournament', function($table) {
        $table->unique(['golfer_id', 'tournament_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
         Schema::table('golfer_tournament', function($table) {
             $table->dropUnique(['golfer_id', 'tournament_id']);
         });
     }
}
