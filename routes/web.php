<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tournaments', 'TournamentsController@index');

Route::get('/tournaments/{tournament}', 'TournamentsController@show');

Route::get('/tournaments/{tournament}/edit', 'TournamentsController@edit');

Route::post('/tournaments/{tournament}/edit', 'TournamentsController@store');

Route::get('/leaderboard', 'HomeController@leaderboard');

Route::get('/picks', 'PicksController@index');

Route::get('/picks/{tournament}', 'PicksController@show');

Route::post('/picks','PicksController@store');
