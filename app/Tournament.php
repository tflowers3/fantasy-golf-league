<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $table = 'tournaments';
    protected $casts = [
        'id' => 'string',
    ];

    protected $fillable = [
        'name',
    ];

    public function golfers()
    {
        return $this->belongsToMany('App\Golfer')->withPivot('id','golfer_id','tournament_id','place','earnings')->orderBy('earnings','DESC')->orderBy('name', 'ASC');
    }

    public function getAllTournaments()
    {
      // Pull all tournaments from pgatour.com and store in DB
      // $client = new Client();
      // $res = $client->get('http://www.pgatour.com/data/r/current/schedule-v2.json');
      // $tournaments = json_decode($res->getBody(),true);
      //
      // $tournaments = $tournaments['years'][0]['tours'][0]['trns'];
      //
      // $data = array();
      // foreach($tournaments as $key => $tournament):
      //   $data[$key]['id'] = $tournament['permNum'];
      //   $data[$key]['name'] = $tournament['trnName']['official'];
      //   $data[$key]['purse'] = $tournament['Purse'] ? str_replace( ',', '', $tournament['Purse'] ) : NULL;
      //   $data[$key]['winners_share'] = $tournament['winnersShare'] ? str_replace( ',', '', $tournament['winnersShare'] ) : NULL;
      //   $data[$key]['date'] = $tournament['date']['start'];
      //   $data[$key]['created_at'] = \Carbon\Carbon::now();
      //   $data[$key]['updated_at'] = \Carbon\Carbon::now();
      // endforeach;
      //
      // \DB::table('tournaments')->insert($data);
    }

    public function getFieldAndResults()
    {
      // Pull field from pgatour.com and store in DB
      // $client = new Client();
      // $tournament_id = '025';
      // $url = 'http://www.pgatour.com/data/r/' . $tournament_id . '/2017/leaderboard-v2mini.json';
      // $res = $client->get($url);
      // $golfers_tournament = json_decode($res->getBody(),true);
      // $golfers_tournament_arr = $golfers_tournament['leaderboard']['players'];
      // $data_golfer = array();
      // $data_golfer_tournament = array();
      // foreach($golfers_tournament_arr as $key => $player):
      //     $data_golfer_tournament[$key]['golfer_id'] = $player['player_id'];
      //     $data_golfer_tournament[$key]['tournament_id'] = $tournament_id;
      //     $data_golfer_tournament[$key]['place'] = $player['current_position'];
      //     $data_golfer_tournament[$key]['earnings'] = $player['rankings']['projected_money_event'];
      //     $data_golfer_tournament[$key]['created_at'] = \Carbon\Carbon::now();
      //     $data_golfer_tournament[$key]['updated_at'] = \Carbon\Carbon::now();
      //
      //     $golfer_id = $player['player_id'];
      //     $name = $player['player_bio']['first_name'] . ' ' . $player['player_bio']['last_name'];
      //     $timestamp = \Carbon\Carbon::now();
      //     Golfer::firstOrCreate(['id' => $golfer_id],['name' => $name],['created_at' => $timestamp],['updated_at' => $timestamp]);
      // endforeach;
      // \DB::table('golfer_tournament')->insert($data_golfer_tournament);
    }
}
