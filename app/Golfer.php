<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golfer extends Model
{
    protected $table = 'golfers';
    protected $fillable = ['id', 'name', 'created_at', 'updated_at'];

    public function tournaments()
    {
        return $this->belongsToMany('App\Tournament')->withPivot('id','golfer_id','tournament_id','place','earnings');
    }

    public function getAllGolfers()
    {
      // Pull all golfers from pgatour.com and store in DB
      // $client = new Client();
      // $res = $client->get('http://www.pgatour.com/content/pgatour/players/jcr:content/mainParsys/players_directory.players.json');
      // $players = json_decode($res->getBody(),true);
      // $players_arr = $players['players'];
      // foreach($players_arr as $key => $player):
      //   if(substr($players_arr[$key]['name'],0,3) == 'HDP'){
      //     unset($players_arr[$key]);
      //   }else{
      //     unset($players_arr[$key]['urlName']);
      //     unset($players_arr[$key]['country']);
      //     unset($players_arr[$key]['countryName']);
      //   }
      // endforeach;
      //
      // \DB::table('golfers')->insert($players_arr);
    }
}
