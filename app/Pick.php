<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pick extends Model
{
    protected $table = 'picks';

    protected $fillable = [
        'golf_tournament_id',
    ];

    public function pick()
    {
        return $this->hasOne('App\User');
    }
}
