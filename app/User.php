<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function savePick(Pick $pick)
    {
      $this->picks()->save($pick);
    }

    public function full_name()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function picks()
    {
        return $this->hasMany('App\Pick')
        ->join('golfer_tournament', 'picks.golfer_tournament_id', '=', 'golfer_tournament.id')
        ->join('golfers', 'golfer_tournament.golfer_id', '=', 'golfers.id')
        ->join('tournaments', 'golfer_tournament.tournament_id', '=', 'tournaments.id')
        ->select('golfers.name AS golfer','tournaments.name AS tournament','golfer_tournament.earnings');
    }

    public function unpicked()
    {
      return DB::table('tournaments')
             ->select('tournaments.date','tournaments.id','tournaments.name')
             ->whereNotIn('tournaments.id', function ($query){
              $query->select(DB::raw('distinct(tournaments.id)'))
                    ->from('tournaments')
                    ->join('golfer_tournament','tournaments.id','=','golfer_tournament.tournament_id')
                    ->join('picks','golfer_tournament.id','=','picks.golfer_tournament_id')
                    ->where('picks.user_id','=',1);
              })
            ->orderBy('date','ASC')
            ->get();
    }
}
