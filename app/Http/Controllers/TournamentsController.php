<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tournament;
use App\Golfer;
use DB;

class TournamentsController extends Controller
{
  public function index()
  {
    $tournaments = Tournament::orderBy('date')->get();
    return view('tournaments.index', compact('tournaments'));
  }

  public function show(Tournament $tournament)
  {
    $golfers = $tournament->golfers;
    return view('tournaments.show', compact('tournament','golfers'));
  }

  public function edit(Tournament $tournament)
  {
    $all_golfers = Golfer::orderBy('name')->get();
    $all_golfer_arr = json_decode($all_golfers,true);
    $cols_all_golfers = array_chunk($all_golfer_arr, ceil(count($all_golfer_arr)/3));

    $tournament_golfers = $tournament->golfers;
    return view('tournaments.edit', compact('tournament','cols_all_golfers','tournament_golfers'));
  }

  public function store(Tournament $tournament)
  {
    $this->validate(request(),[
      'golfer_id' => 'required',
    ]);

    foreach($_POST['golfer_id'] as $golfer_id){
      $golfers[] =  $golfer_id;
    }

    $tournament->golfers()->syncWithoutDetaching($golfers);
    
    return redirect()->back();
  }
}
