<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pick;
use App\User;
use App\Tournament;
use Auth;

class PicksController extends Controller
{
  public function index()
  {
    $picks = Auth::user()->picks;
    $unpicked = Auth::user()->unpicked();
    return view('picks.index', compact('picks','unpicked'));
  }

  public function show(Tournament $tournament)
  {
    $golfers = $tournament->golfers;
    $golfer_arr = json_decode($golfers,true);
    $cols = array_chunk($golfer_arr, ceil(count($golfer_arr)/3));
    return view('picks.show', compact('tournament','cols'));
  }

  public function store()
  {
    $this->validate(request(),[
      'golfer_tournament_id' => 'required',
    ]);

    $pick = new Pick();
    $pick->golfer_tournament_id = $_POST['golfer_tournament_id'];
    auth()->user()->savePick($pick);

    return redirect('/picks');
  }
}
