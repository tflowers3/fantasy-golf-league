<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Golfer;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('home');
    }

    public function leaderboard()
    {
      $players = DB::table('picks')
            ->selectRaw('CONCAT(users.first_name, " ", users.last_name) as player, sum(earnings) as total')
            ->join('golfer_tournament','picks.golfer_tournament_id','=','golfer_tournament.id')
            ->join('users','picks.user_id','=','users.id')
            ->groupBy('picks.user_id')
            ->orderBy('total','DESC')
            ->get();
      return view('picks.leaderboard',compact('players'));
    }
}
