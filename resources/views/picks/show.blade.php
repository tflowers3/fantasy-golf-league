@extends ('layouts.app')

@section ('content')

<form method="post" action="/picks" id="make-pick">
  {{ csrf_field() }}
  <h2 class="mb-5">{{$tournament->name}}</h2>
  <h4>Select Your Golfer</h4>
  <button type="submit" class="btn btn-success float-right">Submit</button>
    <div class="row">
      @foreach ($cols as $golfers)
          <div class="col-sm-4">
            <ul class="list-group">
              @foreach ($golfers as $g)
                <li class="list-group-item">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="golfer_tournament_id" value="{{ $g['pivot']['id'] }}">
                    {{ $g['name'] }}
                  </label>
                </li>
              @endforeach
            </ul>
          </div>
      @endforeach
    </div>
  </form>

@endsection
