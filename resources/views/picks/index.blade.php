@extends('layouts.app')

@section('content')
  <h2>My Picks</h2>
  <div class="table-responsive">
    <table class="table table-striped table-hover table-clickable">
      <thead>
        <tr>
          <th>Tournament</th>
          <th>Golfer</th>
          <th>Earnings</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($picks as $pick)
          <tr>
            <td>{{ $pick->tournament }}</td>
            <td>{{ $pick->golfer }}</td>
            <td>${{ number_format($pick->earnings) }}</td>
          </tr>
        @endforeach

      </tbody>
    </table>
  </div>

  <h2 class="mt-4">Upcoming Tournaments</h2>
  <div class="table-responsive">
    <table class="table table-striped table-hover table-clickable">
      <thead>
        <tr>
          <th>Date</th>
          <th>Tournament</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($unpicked as $tournament)
          <tr data-id="{{ $tournament->id }}">
            <td>{{ Carbon\Carbon::parse($tournament->date)->format('M j') }}</td>
            <td>{{ $tournament->name }}</td>
          </tr>
        @endforeach

      </tbody>
    </table>
  </div>

@endsection
