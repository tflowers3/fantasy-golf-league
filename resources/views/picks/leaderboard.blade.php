@extends('layouts.app')

@section('content')

  <h2>Leaderboard</h2>
  <div class="table-responsive">
    <table class="table table-striped table-hover table-clickable">
      <thead>
        <tr>
          <th>#</th>
          <th>Player</th>
          <th>Total Earnings</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($players as $player)
          <tr>
            <td></td>
            <td>{{ $player->player }}</td>
            <td>${{ number_format($player->total) }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
