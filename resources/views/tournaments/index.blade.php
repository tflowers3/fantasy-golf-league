@extends('layouts.app')

@section('content')
  <h2>Tournaments</h2>
  <div class="table-responsive">
    <table class="table table-striped table-hover table-clickable">
      <thead>
        <tr>
          <th>Date</th>
          <th>Tournament</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($tournaments as $tournament)
          <tr data-id="{{ $tournament->id }}">
            <td>{{ Carbon\Carbon::parse($tournament->date)->format('M j') }}</td>
            <td>{{ $tournament->name }}</td>
          </tr>
        @endforeach

      </tbody>
    </table>
  </div>

@endsection
