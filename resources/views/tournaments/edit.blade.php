@extends ('layouts.app')

@section ('content')

  <h2>{{$tournament->name}}</h2>
  <div class="table-responsive">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Place</th>
          <th>Earnings</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($tournament_golfers as $golfer)
          <tr data-id="{{ $golfer->id }}">
            <td>{{ $golfer->name }}</td>
            <td>{{ $golfer->pivot->place }}</td>
            <td>${{ number_format($golfer->pivot->earnings) }}</td>
            <td>
              <button type="button" class="btn btn-primary btn-sm">Edit</button>
              <button type="button" class="btn btn-danger btn-sm">Delete</button>
            </td>
          </tr>
        @endforeach

      </tbody>
    </table>
  </div>

  <form method="post" action="/tournaments/{{ $tournament->id }}/edit" id="edit-tournament">
    {{ csrf_field() }}
    <h2 class="mt-5">Add Golfers to the Field</h2>
    <button type="submit" class="btn btn-success float-right">Submit</button>
      <div class="row">
        @foreach ($cols_all_golfers as $golfers)
            <div class="col-sm-4">
              <ul class="list-group">
                @foreach ($golfers as $g)
                  <li class="list-group-item">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" name="golfer_id[]" value="{{ $g['id'] }}">
                      {{ $g['name'] }}
                    </label>
                  </li>
                @endforeach
              </ul>
            </div>
        @endforeach
      </div>
    </form>

@endsection
