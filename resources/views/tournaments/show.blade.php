@extends ('layouts.app')

@section ('content')

  <div class="row">
    <div class="col-sm-8">
      <h2>{{$tournament->name}}</h2>
    </div>
    <div class="col-sm-4">
      <a class="float-right" href="/tournaments/{{ $tournament->id }}/edit">Edit</a>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Place</th>
          <th>Earnings</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($golfers as $golfer)
          <tr data-id="{{ $golfer->id }}">
            <td>{{ $golfer->name }}</td>
            <td>{{ $golfer->pivot->place }}</td>
            <td>${{ number_format($golfer->pivot->earnings) }}</td>
          </tr>
        @endforeach

      </tbody>
    </table>
  </div>

@endsection
